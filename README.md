![Image](http://i.imgur.com/Y5ar60s.png)

# build Status

[![build status](https://gitlab.com/senjuana/EDD/badges/gh-pages/build.svg)](https://gitlab.com/senjuana/EDD/commits/gh-pages)

# About
Este  es un repositorio publico que existe para poder guardar los topicos programados en la clase de Estructura de datos y algunos topicos que no fueron tocados en la clase pero si corresponden a la Estructura de datos.


## Requirements

Supported operating systems:

* Development environments: GNU/Linux, MacOS X.

Requirements:

* Cmake

## Getting the code

    $ git clone https://github.com/senjuana/EDD.git
    $ cd EDD


## Compilando
Para compilar las implementaciones dentro de este repositorio, necesitas tener Cmake instalado.
en el siguiente ejemplo compilaremos una de las pilas.    

    $ cd pila
    $ cd pila_nodo
    $ make
    $ ./pila

# Contributing
# Contributing
Este codigo sigue nuestra version de Contributor Covenant antes de hacer aportes al codigo lee con atencion las reglas.
[Contributing](https://gitlab.com/senjuana/EDD/blob/master/CONTRIBUTING.md)



# License

    EDD v1.8 - source code for the EDD application
    Copyright (C) 2016-2017 Ernesto Ruiz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


# FAQ

* **Cual es el punto de compartir tu codigo?**

    Mi unico interes es el de poder compartir mis implementaciones de las estructuras de datos como  herramnienta de aprendizaje.

* **Puedo utilizar este codigo en mis proyectos?**

    Siempre y cuando sigas los parametros de la licencia cualquiera puede utilizar o modiificar las implementaciones de este repositorio para cualquier proyecto personal.

* **Existe alguna documentacion teorica de los conceptos?**

    Si claro dentro del repositorio se creo una documentacion teoria de todos los conceptos utilizados
    por si te falta reafirmar los conceptos teoricos de las estructuras, todo lo puedes revisar en la 
    [Wiki](https://gitlab.com/senjuana/EDD/wikis/home) del repositorio

# Version Notes

Primera version (12/marzo/2016)
version 1.0
[notas de version](https://gitlab.com/senjuana/EDD/tags/v1.0)

Pre-release
version 1.8
[notas de version](https://gitlab.com/senjuana/EDD/tags/v1.8)
