#ifndef __cola
#define __cola

template < class T > class cola {
  	private:
	//Estrucctura Nodo // Mas facil que manjar una clase nodo	
   	typedef struct snode{
		T data;
		snode *next;
	}Nodo;
	// Atributos de la clase capacidad, tamaño y punteros:
   	Nodo *start;
   	Nodo *endcol;
   	int n;
   	int s;
	//Metodos auxiliares
	void borrar( Nodo *& );
	bool isFull();
	bool isEmpty();
 
	public:
  	// Constructor y destructor basicos:
   	cola( const T& );//ingresa la capacidad de la cola 
  	~cola();

   	bool Enqueue( const T& );//ingresas una variable de la cual la funcion toma su valor desde la memoria
   	bool Dequeue();
	bool front( const T& );//la variable que paso como parametro tendra el valor frontal de la cola 
   	void printQueue();
};

#include "imple_col.tpp"
#endif//fin __cola
